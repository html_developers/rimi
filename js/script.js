
var startX = -1;

$(document).ready(function() {
   resizer();
   
   
   $(window).resize(function(){
      resizer();
   });

   $('.more-button').click(function(){
      $(this).toggleClass("active");
      $(this).closest('.one-row').find('.product-list').toggle();
   });

   $('.more-button-level').click(function(){
      $(this).toggleClass("active");
      $(this).closest('.one-level').find('.level2').toggle();
   });


   $('.form-reg .blue-button').click(function(){
      $(this).closest('form').submit();
   });

   $('.plus').click(function(ev){
      ev.stopImmediatePropagation();
      var cd = $(this).closest('.counter-basket').find('.count'),
         c = parseInt(cd.text(),10);
      c++;
      cd.text(c);
      recalcBasket();
   });

   $('.minus').click(function(ev){
      ev.stopImmediatePropagation();
      var cd = $(this).closest('.counter-basket').find('.count'),
         c = parseInt(cd.text(),10);
      c--;
      if (c<1){
         return;
      }
      cd.text(c);
      recalcBasket();
   });

   $('.tab').click(function(){
      $('.tab-content').hide();
      $('#'+$(this).attr('opentab')).show();
      $('.tab').removeClass('active');
      $(this).addClass('active');
   });


   $('.right-level').click(function(){
      var el = $(this),
         rel = el.parent(),
         sl = rel.find('.all-sliders'),
         allsl = rel.find('.level-one-sl').length,
         num = parseInt(sl.attr("num"),10)+1;

      if (allsl>num+4) {
         var ml = -180 * num;
         sl.attr("num", num);
         sl.stop().animate({"margin-left": ml + "px"});
      }
   });

   $('.left-level').click(function(){
      var el = $(this),
         rel = el.parent(),
         sl = rel.find('.all-sliders'),
         num = parseInt(sl.attr("num"),10)-1;

      if (num>=0) {
         var ml = -180 * num;
         sl.attr("num", num);
         sl.stop().animate({"margin-left": ml + "px"});
      }
   });

   $('.arr_left').click(function(){
      var el = $(this),
         sl = el.parent(),
         allsl = sl.find('.slide').length,
         num = parseInt(sl.attr("num"),10)-1;



      if (num<0){
         num=allsl-1;
      }

      sl.find('.slide').stop().animate({opacity:0});
      sl.find('.slide').eq(num).animate({opacity:1});

      sl.find('.one-point').removeClass("active");
      sl.find('.one-point').eq(num).addClass("active");

   });

   $('.arr_right').click(function(){
      var el = $(this),
         sl = el.parent(),
         allsl = sl.find('.slide').length,
         num = parseInt(sl.attr("num"),10)+1;



      if (num>allsl-1){
         num=0;
      }

      sl.find('.slide').stop().animate({opacity:0});
      sl.find('.slide').eq(num).animate({opacity:1});

      sl.find('.one-point').removeClass("active");
      sl.find('.one-point').eq(num).addClass("active");

      sl.attr("num", num);


   });

   $('.one-point').click(function(){
      var el = $(this),
         sl = el.parent().parent(),
         num = parseInt(el.attr("num"),10);
      sl.find('.slide').stop().animate({opacity:0});
      sl.find('.slide').eq(num).animate({opacity:1});

      sl.find('.one-point').removeClass("active");
      sl.find('.one-point').eq(num).addClass("active");

      sl.attr("num", num);
   });

   $('.service').click(function(){
      var a  = $(this).find('a');

      if (a.length>0){
         location.href = a.attr("href");
      }
   });

   var bp = $('.big-photo');
   for (var i=0;i<bp.length;i++){
      var img = bp.eq(i).find('img');
      bp.eq(i).css("background", "url('"+img.attr('src')+"') no-repeat");
      img.remove();
   }
   
   $('.preview').click(function(){
      $(this).closest('.photogallery').find('.big-photo').css("background", "url('"+$(this).attr('big')+"') no-repeat");
   });
   
   $('.star').click(function(){
      $(this).toggleClass("active");
   });

   $('.delete-button').click(function(){
      $(this).closest('.one-product').remove();
      recalcBasket();
   });

   $('.to-basket').click(function(){
		$(this).text("ТОВАР ОТЛОЖЕН");
	});
   
   $('.form-input').focus(function(){
      var def = $(this).attr("def"),
         val = $(this).val();
      if (def === val){
         $(this).val('');
      }
   });
   
   

   $('.form-input').blur(function(){
      var def = $(this).attr("def"),
         val = $(this).val();
      if ( !val){
         $(this).val(def);
      }
   });
   
   $('.dop-info').focus(function(){
      var def = $(this).attr("def"),
         val = $(this).val();
      if (def === val){
         $(this).val('');
      }
   });

   $('.plus-dop').click(function(){
      $(this).toggleClass("active");
      
      var tc = $(this).closest('.tab-content'),
         alldopPr = 0,
         alldop = tc.find('.plus-dop.active');
         
      for (var i=0;i<alldop.length;i++){
         var k = alldop.eq(i).prev(),
            pr = parseInt(k.find('.price-dop').text().replace(/ /g,""),10);
            
         alldopPr += pr;
      }
      
      var basePr = $('.price1').eq(0).text();
      basePr = parseInt(basePr.replace(/ /g,""),10) + alldopPr;
      
      var str = String(basePr);
      str = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
      
      $('.price1').eq(1).find('span').text(str+" РУБ");
      
   });
   
   $('.del-bas-button').click(function(){
      $(this).closest('.one-row').remove();
      recalcBasket();
      
      
   });

   $('.one-color').hover(function(){
      var el = $(this),
         offset = el.offset(),
         div = $('<div class="big-color"><div class="col1"></div></div>');

      $('body').append(div);
      div.css({"left":offset.left+5+"px","top":offset.top-147+"px"});
      div.find('.col1').css({"background":el.css("background")});
   }, function(){
      $('.big-color').remove();
   });


   $('.one-color').click(function(){
      var cr = $(this).closest('.color-range');
      
      cr.find('.one-color').removeClass('active');
      $(this).addClass('active');
      
      cr.find('input').val($(this).attr('id'));
   });


   $('.dop-info').blur(function(){
      var def = $(this).attr("def"),
         val = $(this).val();
      if ( !val){
         $(this).val(def);
      }
   });
   
   $('form').submit(function(){
      var em = $(this.email);
      return validate(em.val());
   });
   
   $('.submit').click(function(){
      $(this).closest('form').submit();
   });
   
   $('.phone-in').keydown(function(ev){
   
      if (ev.keyCode!==9 && (ev.keyCode<48 || ev.keyCode>57))
         return false;
   });
   
   $("a.one-sl").fancybox({
      'transitionIn'		: 'none',
      'transitionOut'	: 'none',
      'titlePosition' 	: 'over'
   });

	
});

function validate(address) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   if(reg.test(address) == false) {
      alert('Введите корректный e-mail');
      return false;
   }
}

function resizer(){

}

function recalcBasket(){
   var r = $('.one-row'),
      alls = 0;

   for(var i=0;i<r.length;i++){
      r.eq(i).find('.col1 span').text(i+1);
      var pl = r.eq(i).find('.product-list');
      if (pl.length>0){
         var prOne = 0;
         var allpr = pl.find('.price-product');
         for (var j=0;j<allpr.length;j++){
            var p1 = allpr.eq(j).text();
            p1 = parseInt(p1.replace(/ /g,""),10);
            prOne += p1;
         }
         var kkk = String(prOne);
         kkk = kkk.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

         r.eq(i).find('.col2').text(kkk+" РУБ");
      }

      var onepr = r.eq(i).find('.col2').text();

      onepr = parseInt(onepr.replace(/ /g,""),10);
      var count = parseInt(r.eq(i).find('.count').text(),10);
      var newSum = onepr*count;

      alls += newSum;

      var str = String(newSum);
      str = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

      r.eq(i).find('.col4').text(str+" РУБ");
   }
   
   $('.count-pr').text(r.length);

   var str = String(alls);
   str = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

   $('.all-price').text(str+"");
}